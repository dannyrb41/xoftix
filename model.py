class Alumno(models.Model):
	nombre = models.CharField(max_length=50)
	grupo = models.CharField(max_length=50)



	def __str__(self):
		return '{} {}'.format(self.nombre, self.grupo)

class Practica(models.Model):

	curso = models.CharField(max_length=5)
	fecha = models.DateField()
	alumno = models.ForeignKey(Alumno, on_delete=models.CASCADE)

	def __str__(self):
		return '{} {}'.format(self.curso, self.fecha)

class Entrega(models.Model):
	practica = models.ForeignKey(Practica, on_delete=models.CASCADE)
	nota = models.FloatField()

	def __str__(self):
		return '{} {}'.format(self.practica,self.nota)
		
		
		
		
		Danny Burbano


Para la solución planteada se necesita Django

tener instalado 

para utilizar mysql se debe utilizar Xampp e  instalar:
	pip install cymysql
	pip install django-cymysql  dentro de Django


se crea un superusuario
	python3 manage.py createsuperuser
	te pedira un nombre de user:___ correo electronico____ password y repetir password

usuario por defecto admin contraseña 123456


en phpmyadmin crea la BD colegio

y cargar los siguientes datos 






BASE DE DATOS




-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 10-08-2019 a las 18:47:41
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colegio`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add alumno', 7, 'add_alumno'),
(26, 'Can change alumno', 7, 'change_alumno'),
(27, 'Can delete alumno', 7, 'delete_alumno'),
(28, 'Can view alumno', 7, 'view_alumno'),
(29, 'Can add practica', 8, 'add_practica'),
(30, 'Can change practica', 8, 'change_practica'),
(31, 'Can delete practica', 8, 'delete_practica'),
(32, 'Can view practica', 8, 'view_practica'),
(33, 'Can add entrega', 9, 'add_entrega'),
(34, 'Can change entrega', 9, 'change_entrega'),
(35, 'Can delete entrega', 9, 'delete_entrega'),
(36, 'Can view entrega', 9, 'view_entrega');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$150000$JCM4Wu9BLFoi$ROiMhpaRz/mrP9sW4zhvKUJOLv2EcvD1O2P6S8ILR9w=', '2019-08-10 16:22:49.578000', 1, 'admin', '', '', '', 1, 1, '2019-08-10 16:22:29.723004');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colegio_alumno`
--

CREATE TABLE `colegio_alumno` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `grupo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colegio_alumno`
--

INSERT INTO `colegio_alumno` (`id`, `nombre`, `grupo`) VALUES
(1, 'karol lopez', 'segundo grupo'),
(2, 'juan Lopez', 'segundo grupo'),
(3, 'nanci rodriguez', 'segundo grupo'),
(4, 'diana marcillo', 'segundo grupo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colegio_entrega`
--

CREATE TABLE `colegio_entrega` (
  `id` int(11) NOT NULL,
  `nota` double NOT NULL,
  `practica_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colegio_entrega`
--

INSERT INTO `colegio_entrega` (`id`, `nota`, `practica_id`) VALUES
(1, 3.5, 2),
(2, 5, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colegio_practica`
--

CREATE TABLE `colegio_practica` (
  `id` int(11) NOT NULL,
  `curso` varchar(5) NOT NULL,
  `fecha` date NOT NULL,
  `alumno_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `colegio_practica`
--

INSERT INTO `colegio_practica` (`id`, `curso`, `fecha`, `alumno_id`) VALUES
(1, '5', '2019-08-10', 1),
(2, '7', '2019-08-10', 3),
(3, '9', '2019-08-10', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-08-10 16:39:05.120523', '1', 'karol lopez segundo grupo', 1, '[{\"added\": {}}]', 7, 1),
(2, '2019-08-10 16:39:20.686793', '2', 'juan Lopez segundo grupo', 1, '[{\"added\": {}}]', 7, 1),
(3, '2019-08-10 16:39:33.118492', '3', 'nanci rodriguez segundo grupo', 1, '[{\"added\": {}}]', 7, 1),
(4, '2019-08-10 16:39:56.468750', '4', 'diana marcillo segundo grupo', 1, '[{\"added\": {}}]', 7, 1),
(5, '2019-08-10 16:40:54.762503', '1', '5 2019-08-10', 1, '[{\"added\": {}}]', 8, 1),
(6, '2019-08-10 16:41:08.735040', '2', '7 2019-08-10', 1, '[{\"added\": {}}]', 8, 1),
(7, '2019-08-10 16:41:20.362123', '3', '9 2019-08-10', 1, '[{\"added\": {}}]', 8, 1),
(8, '2019-08-10 16:42:04.315048', '1', '7 2019-08-10 3.5', 1, '[{\"added\": {}}]', 9, 1),
(9, '2019-08-10 16:42:16.038227', '2', '9 2019-08-10 5.0', 1, '[{\"added\": {}}]', 9, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(7, 'colegio', 'alumno'),
(9, 'colegio', 'entrega'),
(8, 'colegio', 'practica'),
(5, 'contenttypes', 'contenttype'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-08-10 16:21:57.684059'),
(2, 'auth', '0001_initial', '2019-08-10 16:21:58.206899'),
(3, 'admin', '0001_initial', '2019-08-10 16:21:59.809584'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-08-10 16:22:00.162372'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2019-08-10 16:22:00.193463'),
(6, 'contenttypes', '0002_remove_content_type_name', '2019-08-10 16:22:00.499743'),
(7, 'auth', '0002_alter_permission_name_max_length', '2019-08-10 16:22:00.675150'),
(8, 'auth', '0003_alter_user_email_max_length', '2019-08-10 16:22:00.850165'),
(9, 'auth', '0004_alter_user_username_opts', '2019-08-10 16:22:00.880024'),
(10, 'auth', '0005_alter_user_last_login_null', '2019-08-10 16:22:00.984540'),
(11, 'auth', '0006_require_contenttypes_0002', '2019-08-10 16:22:00.993645'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2019-08-10 16:22:01.021497'),
(13, 'auth', '0008_alter_user_username_max_length', '2019-08-10 16:22:01.248478'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2019-08-10 16:22:01.458280'),
(15, 'auth', '0010_alter_group_name_max_length', '2019-08-10 16:22:01.654671'),
(16, 'auth', '0011_update_proxy_permissions', '2019-08-10 16:22:01.690699'),
(17, 'colegio', '0001_initial', '2019-08-10 16:22:01.892159'),
(18, 'sessions', '0001_initial', '2019-08-10 16:22:02.300595');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('kai97a5jq71jzxbdm9bz0r5kf8ke16yy', 'ZWJlZjk1ZWFhNGI2ZDJiYjExNjA1MGE5NTg0YjVmYTBiYzNlMDY3OTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzOTcxODI5MzgxZDQ5ZjQxZDY3YTgxYzI4NjkyOTM3NWE4Njg5YzhkIn0=', '2019-08-24 16:22:49.592504');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `colegio_alumno`
--
ALTER TABLE `colegio_alumno`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colegio_entrega`
--
ALTER TABLE `colegio_entrega`
  ADD PRIMARY KEY (`id`),
  ADD KEY `colegio_entrega_practica_id_ecf53b32_fk_colegio_practica_id` (`practica_id`);

--
-- Indices de la tabla `colegio_practica`
--
ALTER TABLE `colegio_practica`
  ADD PRIMARY KEY (`id`),
  ADD KEY `colegio_practica_alumno_id_7f9ccf4a_fk_colegio_alumno_id` (`alumno_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `colegio_alumno`
--
ALTER TABLE `colegio_alumno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `colegio_entrega`
--
ALTER TABLE `colegio_entrega`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `colegio_practica`
--
ALTER TABLE `colegio_practica`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `colegio_entrega`
--
ALTER TABLE `colegio_entrega`
  ADD CONSTRAINT `colegio_entrega_practica_id_ecf53b32_fk_colegio_practica_id` FOREIGN KEY (`practica_id`) REFERENCES `colegio_practica` (`id`);

--
-- Filtros para la tabla `colegio_practica`
--
ALTER TABLE `colegio_practica`
  ADD CONSTRAINT `colegio_practica_alumno_id_7f9ccf4a_fk_colegio_alumno_id` FOREIGN KEY (`alumno_id`) REFERENCES `colegio_alumno` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;